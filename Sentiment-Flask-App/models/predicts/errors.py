
class PredictException(Exception):
    def __init__(self, message):
        self.message = message


class PredictNotFoundException(PredictException):
    pass