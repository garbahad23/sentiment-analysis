from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
import pickle

import pandas as pd
import numpy as np


class LogisticModel(object):

    DF = None
    VECT = None
    FILENAME = None
    LOADED_MODEL = None

    @staticmethod
    def initialize():
        # load np dataframe from sentiment data
        LogisticModel.DF = pd.read_csv('predictive_data/logistic/sentimentdataset.csv', encoding='utf-8', sep=',')
        # create test and train data
        X_train, X_test, y_train, y_test = train_test_split(
            LogisticModel.DF['yorum'], LogisticModel.DF['Positivity'], random_state=42, test_size=0.05, shuffle=True)
        # CountVectorizer'ı başlatıyoruz ve eğitim verilerimize uyguluyoruz.
        LogisticModel.VECT = CountVectorizer(encoding='iso-8859-9').fit(X_train)

        # load sentiment the model to disk
        LogisticModel.FILENAME = 'predictive_models/finalized_model.sav'
        # load the model from disk
        LogisticModel.LOADED_MODEL = pickle.load(open(LogisticModel.FILENAME, 'rb'))

    @staticmethod
    def logistic_predict(rawtext):
        textArray = [rawtext]
        sentiment_prediction = LogisticModel.LOADED_MODEL.predict(LogisticModel.VECT.transform(textArray))

        return sentiment_prediction