# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np

df = pd.read_csv('sentimentdataset.csv', encoding ='utf-8', sep=',')
df.shape
df.head()

df.columns

import spacy
from spacy.lang.tr.stop_words import STOP_WORDS
nlp = spacy.load('en')

stopwords = list(STOP_WORDS)
stopwords

# Use the punctuations of string module
import string
punctuations = string.punctuation

# Creating a Spacy Parser
from spacy.lang.en import English
parser = English()

def spacy_tokenizer(sentence):
    mytokens = parser(sentence)
    mytokens = [ word.lemma_.lower().strip() if word.lemma_ != "-PRON-" else word.lower_ for word in mytokens ]
    mytokens = [ word for word in mytokens if word not in stopwords and word not in punctuations ]
    return mytokens

# ML Packages
from sklearn.feature_extraction.text import CountVectorizer,TfidfVectorizer
from sklearn.metrics import accuracy_score 
from sklearn.base import TransformerMixin 
from sklearn.pipeline import Pipeline
from sklearn.svm import LinearSVC
from sklearn.linear_model import LogisticRegression

#Custom transformer using spaCy 
class predictors(TransformerMixin):
    def transform(self, X, **transform_params):
        return [clean_text(text) for text in X]
    def fit(self, X, y=None, **fit_params):
        return self
    def get_params(self, deep=True):
        return {}

# Basic function to clean the text 
def clean_text(text):     
    return text.strip().lower()

# Vectorization
vectorizer = CountVectorizer(tokenizer = spacy_tokenizer, ngram_range=(1,3)) 
#classifier = LinearSVC()
classifier = LogisticRegression()

# Using Tfidf
tfvectorizer = TfidfVectorizer(tokenizer = spacy_tokenizer)

# Splitting Data Set
from sklearn.model_selection import train_test_split

# Features and Labels
X = df['yorum']
ylabels = df['Positivity']

X_train, X_test, y_train, y_test = train_test_split(X, ylabels, test_size=0.05, random_state=0, shuffle=True)

# Create the  pipeline to clean, tokenize, vectorize, and classify 
pipe = Pipeline([("cleaner", predictors()),
                 ('vectorizer', vectorizer),
                 ('classifier', classifier)])
    
# Fit our data
pipe.fit(X_train,y_train)

# Predicting with a test dataset
sample_prediction = pipe.predict(X_test)

# Prediction Results
# 1 = Positive review
# 0 = Negative review
for (sample,pred) in zip(X_test,sample_prediction):
    print(sample,"Prediction=>",pred)

# Accuracy
print("Accuracy: ",pipe.score(X_test,y_test))
print("Accuracy: ",pipe.score(X_train,y_train))
print("Accuracy: ",pipe.score(X_test,sample_prediction))

# New Data
text1 = "Bu bankayı hiç beğenmedim."
text2 = "seni sikerim piç köpek soyu"
text3 = "amına bile korum orospu çocuğu"
text4 = "hadi ordan yavşak"
text5 = "çok iyi bir banka"
text6 = "faiz oranları çok düşük"
text7 = "hizmet kaliteniz çok kötü"
text8 = "en iyi banka tabiki ing"
text9 = "faiz oranlarınız çok iyi"
text10 = "faiz oranlarınız çok düşük"
texts = [text1, text2, text3, text4, text5, text6, text7, text8, text9, text10]

pipe.predict(texts)


# Save and retrieve model with pickle
import pickle
# save the model to disk
filename = 'model/finalized_model.sav'
pickle.dump(pipe, open(filename, 'wb'))

# load the model from disk
loaded_model = pickle.load(open(filename, 'rb'))
loaded_model.predict(texts)