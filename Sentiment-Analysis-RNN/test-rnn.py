import tensorflow as tf
import os
from sklearn.preprocessing import LabelBinarizer
import pickle

from tensorflow.python.keras.preprocessing.text import Tokenizer
from tensorflow.python.keras.preprocessing.sequence import pad_sequences

import pandas as pd
import numpy as np


model = tf.keras.models.load_model(
    "model/tf_sentiment_model.hd5",
    custom_objects=None,
    compile=True
)

df = pd.read_csv('sentimentdataset.csv', encoding ='utf-8', sep=',')

from sklearn.model_selection import train_test_split

x_train_text, x_test_text, y_train, y_test = train_test_split(df['yorum'], df['Positivity'], random_state = 0, test_size=0.05,shuffle=True)
print(x_train_text.head())
print('\n\nX_train shape: ', x_train_text.shape)


print("Train-set size: ", len(x_train_text))
print("Test-set size:  ", len(x_test_text))

#Combine into one data-set for some uses below.
frames = [x_train_text,x_test_text]
data_text = pd.concat(frames)

num_words = 10000
tokenizer = Tokenizer(num_words=num_words)
tokenizer.fit_on_texts(data_text)

max_tokens = 71
pad = 'pre'

# New Data
text1 = "Bu bankayı hiç beğenmedim."
text2 = "seni sikerim piç köpek soyu"
text3 = "amına bile korum orospu çocuğu"
text4 = "hadi ordan yavşak"
text5 = "çok iyi bir banka"
text6 = "faiz oranları çok düşük"
text7 = "hizmet kaliteniz çok kötü"
text8 = "en iyi banka tabiki ing"
text9 = "faiz oranlarınız çok iyi"
text10 = "faiz oranlarınız çok düşük"
text11 = "iyiki varsın ing"
text12 = "sizi çok seviyorum"
text13 = "en iyi banka ing bank"
text14 = "hizmet kalitesi çok iyi"
text15 = "ing bank tan çok memnunuz"
text16 = "aq ben sizin"
text17 = "bu bankadan bi sikim olmaz"
text18 = "paramı çaldınız piçler"
text19 = "sizi sevmiyorum"
text20 = "aq"
texts = [text1, text2, text3, text4, text5, text6, text7, text8, text9, text10,
         text11, text12, text13, text14, text15, text16, text17, text18, text19, text20]


tokens = tokenizer.texts_to_sequences(texts)

tokens_pad = pad_sequences(tokens, maxlen=max_tokens,
                           padding=pad, truncating=pad)
tokens_pad.shape

print(model.predict(tokens_pad))

print("\n Yeni datalar ile predict et \n")
# New Data
text1 = "Bu bankayı hiç beğenmedim."
text2 = "seni sikerim piç köpek soyu"
text3 = "amına bile korum orospu çocuğu"
text4 = "hadi ordan yavşak"
text5 = "çok iyi bir banka"
text6 = "faiz oranları çok düşük"
text7 = "hizmet kaliteniz çok kötü"
text8 = "en iyi banka tabiki ing"
text9 = "faiz oranlarınız çok iyi"
text10 = "faiz oranlarınız çok düşük"
text11 = "seni seviyoruz ingo"
text12 = "ingo candır"
text13 = "bugün bankaya bi gidicem"
text14 = "sizi çok seviyorum"
text15 = "bugün neye yatırım yapsam"
text16 = "bugün ne giysem"
text17 = "parayı faize mi yatırsam"
text18 = "en yakın atm nerde"
text19 = "en yakın şube nerde"
text20 = "aq"
text21 = "eft yapmak istiyorum"
text22 = "havale yapmak istiyorum"
text23 = "para göndermek istiyorum"
text24 = "konut faizleri çok düşük"
texts = [text1, text2, text3, text4, text5, text6, text7, text8, text9, text10, 
         text11, text12, text13, text14, text15, text16, text17, text18, text19, 
         text20, text21, text22, text23, text24]

tokens = tokenizer.texts_to_sequences(texts)

tokens_pad = pad_sequences(tokens, maxlen=max_tokens,
                           padding=pad, truncating=pad)
tokens_pad.shape

print(model.predict(tokens_pad))

polarityArray = model.predict(tokens_pad)
print(polarityArray[2][0])

text = "en iyi banka tabiki ing bank"
texts = [text]
tokens = tokenizer.texts_to_sequences(texts)

tokens_pad = pad_sequences(tokens, maxlen=max_tokens,
                           padding=pad, truncating=pad)
tokens_pad.shape
polarityArray = model.predict(tokens_pad)
print(polarityArray[0][0])