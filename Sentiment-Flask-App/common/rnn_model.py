# Tensorflow
import tensorflow as tf
from tensorflow.python.keras.preprocessing.text import Tokenizer
from tensorflow.python.keras.preprocessing.sequence import pad_sequences

import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split


class RnnModel(object):
    graph = None
    rnn_model = None
    df = None
    tokenizer = None

    num_words = 10000
    max_tokens = 71
    pad = 'pre'

    @staticmethod
    def initialize():
        RnnModel.graph = tf.get_default_graph()
        RnnModel.rnn_model = tf.keras.models.load_model(
            "predictive_models/tf_sentiment_model.hd5",
            custom_objects=None,
            compile=True
        )

        RnnModel.df = pd.read_csv('predictive_data/rnn/sentimentdataset.csv', encoding='utf-8', sep=',')
        x_train, x_test, y_train, y_test = train_test_split(
            RnnModel.df['yorum'], RnnModel.df['Positivity'], random_state=0, test_size=0.05, shuffle=True)
        # Combine into one data-set for some uses below.
        frames = [x_train, x_test]
        data_text = pd.concat(frames)

        RnnModel.tokenizer = Tokenizer(num_words=RnnModel.num_words)
        RnnModel.tokenizer.fit_on_texts(data_text)


    @staticmethod
    def rnn_predict(rawtext):
        text = [str(rawtext)]

        tokens = RnnModel.tokenizer.texts_to_sequences(text)
        tokens_pad = pad_sequences(tokens, maxlen=RnnModel.max_tokens,
                                   padding=RnnModel.pad, truncating=RnnModel.pad)
        tokens_pad.shape

        with RnnModel.graph.as_default():
            return RnnModel.rnn_model.predict(tokens_pad)
