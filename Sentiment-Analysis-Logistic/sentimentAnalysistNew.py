#Kütüphanelerin eklenmesi
import pandas as pd
import numpy as np

#DATA PREPROCESSING
#Veri setinin eklenip başlığının belirlenmesi
column = ['yorum']
df = pd.read_csv('yorumlar.csv', encoding ='iso-8859-9', sep='"')
df.columns=column
df.info()
df.head()

#Verisetinde Positivity adlı bir sütunun oluşturalım ve 
#başlangıçta tüm değerlere olumlu olarak 1 değerinin atayalım
df['Positivity'] = 1

#Veri setimizde 10003. veri ve sonrasındaki veriler olumsuz verilerdir bu yüzden bu değerlerin
#Positivity değerleri 0 ile değiştirelim.
df.Positivity.iloc[10003:] = -1

#pozitif kelimelerden oluşan kelime seti
dp = pd.read_csv('pozitif_kelimeler_v2.csv', encoding ='utf-8', sep='"')
dp.columns = column
dp.info()
dp.head()

#negatif kelimelerden oluşan kelime seti
dn = pd.read_csv('negatif_kelimeler_v2.csv', encoding ='utf-8', sep='"')
dn.columns = column
dn.info()
dn.head()

dp['Positivity'] = 1
dn['Positivity'] = -1


#banka sentiment kelimeleri pos,notr,neg
dbs = pd.read_csv('bank-dataset-sentiment.csv', encoding ='utf-8', sep=',')
dbs.info()
dbs.head()
    

# tüm dataframe leri birleştirdik
frames = [df,dn,dp,dbs]
df = pd.concat(frames)
df = df.reset_index(drop=True)

# clean the dataset
df['yorum'] = df['yorum'].str.strip().replace(u'I',u'ı').replace(u'İ',u'i')
df['yorum'] = df['yorum'].str.lower()
df['yorum'] = df['yorum'].str.replace('[-()\"#\@;:<>{}+=~.?|,]','')

# stop word leri çıkardık  
## Veri setindeki Türkçe dolgu kelimelerinin kaldırılması
#stopwords = open('turkce-stop-words', 'r').read().split()
#df['yorum'] = df['yorum'].apply(lambda x: ' '.join([word for word in x.split() if word not in (stopwords)]))

df['yorum'] = df['yorum'].replace('', np.nan)
df = df.dropna(subset=['yorum'])
#df = df.drop_duplicates(['yorum'], keep='last')
df = df.drop_duplicates()
df = df.reset_index(drop=True)

# Export whole set of sentiment analysis data
df.to_csv('sentimentdataset.csv',index=False)
ds = pd.read_csv('sentimentdataset.csv', encoding ='utf-8', sep=',')



# CERATE AND TRAIN MODEL
#Şimdi, verileri "yorum" ve "Positivity" sütunlarını kullanarak rastgele eğitim ve test alt kümelerini
#bölüştürelim ve ardından ilk girişi ve eğitim setinin şeklini yazalım.
from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(df['yorum'], df['Positivity'], random_state=42, test_size=0.05,shuffle=True)
print(X_train.head())
print('\n\nX_train shape: ', X_train.shape)


#CountVectorizer'ı başlatıyoruz ve eğitim verilerimize uyguluyoruz.
from sklearn.feature_extraction.text import CountVectorizer
vect = CountVectorizer(encoding ='iso-8859-9').fit(X_train)

#X_train'deki belgeleri bir belge terim matrisine dönüştürürüz
X_train_vectorized = vect.transform(X_train) 


#Bu özellik matrisi X_ train_ vectorized'e dayanarak Lojistik Regresyon sınıflandırıcısını eğiteceğiz
# % 88
from sklearn.linear_model import LogisticRegression
model = LogisticRegression(random_state=42)
model.fit(X_train_vectorized, y_train)


#Daha sonra, X_test kullanarak tahminler yapacağız ve eğri puanının altındaki alanı hesaplayacağız.
from sklearn.model_selection import cross_val_score
from sklearn.metrics import accuracy_score
test_predictions = model.predict(vect.transform(X_test))
accuracy_score(y_test, test_predictions)
# accuracy score for train data
train_predictions = model.predict(X_train_vectorized)
accuracy_score(y_train, train_predictions)

# F1 score test
from sklearn.metrics import f1_score
test_score = f1_score(y_test, test_predictions, average='macro')
print('f1 score test: ' + str(test_score))
# F1 score train
train_score = f1_score(y_train, train_predictions, average='macro')
print('f1 score train: ' + str(train_score))

# Classification Report
from sklearn.metrics import classification_report
print(classification_report(y_test, test_predictions))
# Classification report for train data
print(classification_report(y_train, train_predictions))


# New Data
text1 = "Bu bankayı hiç beğenmedim."
text2 = "seni sikerim piç köpek soyu"
text3 = "amına bile korum orospu çocuğu"
text4 = "hadi ordan yavşak"
text5 = "çok iyi bir banka"
text6 = "faiz oranları çok düşük"
text7 = "hizmet kaliteniz çok kötü"
text8 = "en iyi banka tabiki ing"
text9 = "faiz oranlarınız çok iyi"
text10 = "faiz oranlarınız çok düşük"
text11 = "seni seviyoruz ingo"
text12 = "ingo candır"
text13 = "bugün bankaya bi gidicem"
text14 = "sizi çok seviyorum"
text15 = "bugün neye yatırım yapsam"
text16 = "bugün ne giysem"
text17 = "parayı faize mi yatırsam"
text18 = "en yakın atm nerde"
text19 = "en yakın şube nerde"
text20 = "aq"
text21 = "eft yapmak istiyorum"
text22 = "havale yapmak istiyorum"
text23 = "para göndermek istiyorum"
text24 = "para çekmek istiyorum"
text25 = "beğenmedim"
text26 = "senden daha iyi bir banka bulamadık"
text27 = "çok zekisin be ingo"
text28 = "selam ingo nasılsın"
text29 = "paramı resmen size kaptırdım ayıptır"
text30 = "teşekkür ederim"
text31 = "çok sağol"
text32 = "eft ücretleriniz çok fazla"
text33 = "selam ingo nasılsın"
text34 = "ordamısınız"
text35 = "kredi için ne yapmalıyım"
text36 = "günlük faiz oranı nedir"
texts = [text1, text2, text3, text4, text5, text6, text7, text8, text9, text10, 
         text11, text12, text13, text14, text15, text16, text17, text18, text19, 
         text20, text21, text22, text23, text24, text25, text26, text27, text28,
         text29, text30, text31, text32,text33, text34, text35, text36]


model.predict(vect.transform(texts))

model.predict(vect.transform(['daha iyi banka bulamadım']))

# Save and retrieve model with pickle
import pickle
# save the model to disk
filename = 'model/finalized_model.sav'
pickle.dump(model, open(filename, 'wb'))

# load the model from disk
loaded_model = pickle.load(open(filename, 'rb'))
loaded_model.predict(vect.transform(texts))


#Modelimizin bu tahminleri nasıl yaptığını daha iyi anlamak için, her bir özellik için katsayıları (bir kelime), 
#pozitifliği ve olumsuzluk açısından ağırlığını belirlemek için kullanabiliriz.
feature_names = np.array(vect.get_feature_names())
sorted_coef_index = model.coef_[0].argsort()
print('Negatif: \n{}\n'.format(feature_names[sorted_coef_index[:10]]))
print('Pozitif: \n{}\n'.format(feature_names[sorted_coef_index[:-11:-1]]))


#tf-idf vectorizer'ı başlatacağız ve eğitim verilerimize uygulayacağız. 
#En az beş dokümanda görünen kelime dağarcığımızdaki kelimeleri kaldıracağımız min_df = 5 değerini belirtiyoruz.
from sklearn.feature_extraction.text import TfidfVectorizer
vect = TfidfVectorizer(min_df = 3).fit(X_train)

X_train_vectorized = vect.transform(X_train)
model = LogisticRegression()
model.fit(X_train_vectorized, y_train)
predictions = model.predict(vect.transform(X_test))
print('AUC: ', roc_auc_score(y_test, predictions))  

feature_names = np.array(vect.get_feature_names())
sorted_tfidf_index = X_train_vectorized.max(0).toarray()[0].argsort()
print('En küçük Tfidf: \n{}\n'.format(feature_names[sorted_tfidf_index[:10]]))
print('En büyük Tfidf: \n{}\n'.format(feature_names[sorted_tfidf_index[:-11:-1]]))


#bigramlar, bitişik kelimelerin çiftlerini sayar ve bize kötü ve kötü olmayan gibi özellikler verebilir. 
#Bu nedenle, minimum 5 belge sıklığını belirten ve 1 gram ve 2 gram extract eden 
#eğitim setimizi yeniden yerleştiriyoruz.
vect = CountVectorizer(min_df = 5, ngram_range = (1,2)).fit(X_train)
X_train_vectorized = vect.transform(X_train)


model = LogisticRegression()
model.fit(X_train_vectorized, y_train)
predictions = model.predict(vect.transform(X_test))
print('AUC: ', roc_auc_score(y_test, predictions))

#Her özelliği kontrol etmek için katsayıları kullanarak görebiliriz
feature_names = np.array(vect.get_feature_names())
sorted_coef_index = model.coef_[0].argsort()
print('Negatif: \n{}\n'.format(feature_names[sorted_coef_index][:10]))
print('Pozitif Coef: \n{}\n'.format(feature_names[sorted_coef_index][:-11:-1]))

while(True):
    yorum=input("Cümleyi alalım (Programdan çıkmak için \'Q\' yazınız):")
    if(yorum == 'Q' or yorum == 'q'):
        break
    else:
        print(model.predict(vect.transform([yorum])))