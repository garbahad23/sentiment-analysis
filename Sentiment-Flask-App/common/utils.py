

class Utils(object):

    stopwords = None

    @staticmethod
    def initialize():
        Utils.stopwords = open('predictive_data/turkce-stop-words', 'r').read().split()

    # Veri setindeki Türkçe dolgu kelimelerinin kaldırılması
    @staticmethod
    def remove_stopwords(sentence):
        sentence = ' '.join([word for word in sentence.split() if word not in Utils.stopwords])
        return sentence

    @staticmethod
    def calculate_sentiment_score(meaning, sentiment, polarity):
        sentiment_score = 0
        meaning = int(meaning)
        sentiment = int(sentiment)

        # Cast polarity to sentiment
        polarity = int(polarity * 100)
        if polarity >= 55:
            polarity = 1
        elif 45 < polarity < 55:
            polarity = 0
        elif polarity <= 45:
            polarity = -1
        else:
            polarity = 0

        # calculate overal sentiment score
        if polarity == 0 and sentiment == 0:
            sentiment_score = sentiment
        elif polarity == 1 and sentiment == 1:
            sentiment_score = sentiment
        elif polarity == -1 and sentiment == -1:
            sentiment_score = sentiment
        elif sentiment == 0:
            sentiment_score = sentiment
        else:
            sentiment_score = (sentiment * 3) + polarity + meaning
            if sentiment_score < 0:
                sentiment_score = -1
            elif sentiment_score > 0:
                sentiment_score = 1
            else:
                sentiment_score = 0

        return sentiment_score