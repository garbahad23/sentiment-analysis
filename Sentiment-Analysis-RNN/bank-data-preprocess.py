# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd

# Positive kelimeler
column = ['yorum']
dpos = pd.read_csv('pozitif_bank_kelimeler_v1.csv', encoding ='utf-8', sep='"')
dpos.columns = column
dpos.info()
dpos.head()
dpos['Positivity'] = 1


# Negatif kelimeler
column = ['yorum']
dneg = pd.read_csv('negatif_bank_kelimeler_v1.csv', encoding ='utf-8', sep='"')
dneg.columns = column
dneg.info()
dneg.head()
dneg['Positivity'] = 0



# tüm bank data yı birleştir
frames = [dpos, dneg]
df = pd.concat(frames)

#Veri setindeki Türkçe dolgu kelimelerinin kaldırılması
def remove_stopwords(df_fon):
    stopwords = open('turkce-stop-words', 'r').read().split()
    #df_fon['stopwords_removed'] = list(map(lambda doc:
        #[word for word in doc if word not in stopwords], df_fon['yorum']))
    df_fon['yorum'] = df_fon['yorum'].apply(lambda x: ' '.join([word for word in x.split() if word not in (stopwords)]))
    
        
remove_stopwords(df)

df.to_csv("bank-dataset-sentiment.csv",index=False)



# Boğaziçi notr kelimeler
dn = pd.read_csv('notr-train.csv', encoding ='utf-8', sep='			')
column = ['yorum']
dn.columns = column
dn['Positivity'] = 0

remove_stopwords(dn)

dn.to_csv("notr_kelimeler_v2.csv",index=False)







