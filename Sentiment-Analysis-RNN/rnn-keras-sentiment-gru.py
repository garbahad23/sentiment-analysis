# -*- coding: utf-8 -*-
"""
Created on Sat Sep 15 14:40:14 2018

@author: OKAN
"""

import matplotlib.pyplot as plt
import tensorflow as tf
import numpy as np
from scipy.spatial.distance import cdist

# We need to import several things from Keras.
from tensorflow.python.keras.models import Sequential
from tensorflow.python.keras.layers import Dense, GRU, Embedding
from tensorflow.python.keras.optimizers import Adam
from tensorflow.python.keras.preprocessing.text import Tokenizer
from tensorflow.python.keras.preprocessing.sequence import pad_sequences

print(tf.__version__)
print(tf.keras.__version__)

import pandas as pd
import numpy as np

#Veri setinin eklenip başlığının belirlenmesi
column = ['yorum']
df = pd.read_csv('yorumlar.csv', encoding ='iso-8859-9', sep='"')
df.columns=column
df.info()
df.head()

#pozitif kelimelerden oluşan kelime seti
dp = pd.read_csv('pozitif_kelimeler_v2.csv', encoding ='utf-8', sep='"')
dp.columns = column
dp.info()
dp.head()

#negatif kelimelerden oluşan kelime seti
dn = pd.read_csv('negatif_kelimeler_v2.csv', encoding ='utf-8', sep='"')
dn.columns = column
dn.info()
dn.head()

#banka pozitif sentiment kelimeleri pos
dbp = pd.read_csv('pozitif_bank_kelimeler_v1.csv', encoding ='utf-8', sep=',')
dbp.columns = column
dbp.info()
dbp.head()

#banka negatif sentiment kelimeleri neg
dbn = pd.read_csv('negatif_bank_kelimeler_v1.csv', encoding ='utf-8', sep=',')
dbn.columns = column
dbn.info()
dbn.head()

#Veri setindeki Türkçe dolgu kelimelerinin kaldırılması
def remove_stopwords(df_fon):
    stopwords = open('turkce-stop-words', 'r').read().split()
    #df_fon['stopwords_removed'] = list(map(lambda doc:
        #[word for word in doc if word not in stopwords], df_fon['yorum']))
    df_fon['yorum'] = df_fon['yorum'].apply(lambda x: ' '.join([word for word in x.split() if word not in (stopwords)]))
    
        
remove_stopwords(df)

#Verisetinde Positivity adlı bir sütunun oluşturalım ve 
#başlangıçta tüm değerlere olumlu olarak 1 değerinin atayalım
df['Positivity'] = 1

#Veri setimizde 10003. veri ve sonrasındaki veriler olumsuz verilerdir bu yüzden bu değerlerin
#Positivity değerleri 0 ile değiştirelim.
df.Positivity.iloc[10003:] = 0

#df = df.drop(columns=['stopwords_removed'])
dp['Positivity'] = 1
dn['Positivity'] = 0
dbp['Positivity'] = 1
dbn['Positivity'] = 0

frames = [df,dn,dp]
df = pd.concat(frames)

# Export whole set of sentiment analysis data
df.to_csv("sentimentdataset.csv",index=False)
ds = pd.read_csv('sentimentdataset.csv', encoding ='utf-8', sep=',')

#df = pd.read_csv('sentimentdataset.csv', encoding ='utf-8', sep=',')

from sklearn.model_selection import train_test_split

x_train_text, x_test_text, y_train, y_test = train_test_split(df['yorum'], df['Positivity'], random_state = 0, test_size=0.05,shuffle=True)
print(x_train_text.head())
print('\n\nX_train shape: ', x_train_text.shape)


print("Train-set size: ", len(x_train_text))
print("Test-set size:  ", len(x_test_text))

#Combine into one data-set for some uses below.
frames = [x_train_text,x_test_text]
data_text = pd.concat(frames)

#Print an example from the training-set to see that the data looks correct.
x_train_text[1]

y_train[1]

#Tokenizer

num_words = 10000
tokenizer = Tokenizer(num_words=num_words)
tokenizer.fit_on_texts(data_text)

if num_words is None:
    num_words = len(tokenizer.word_index)

tokenizer.word_index

#We can then use the tokenizer to convert all texts in the training-set to lists of these tokens.
x_train_tokens = tokenizer.texts_to_sequences(x_train_text)

#For example, here is a text from the training-set:
x_train_text[1]

# This text corresponds to the following list of tokens:
np.array(x_train_tokens[1])

#We also need to convert the texts in the test-set to tokens.
x_test_tokens = tokenizer.texts_to_sequences(x_test_text)


#Padding and Truncating Data
#First we count the number of tokens in all the sequences in the data-set.
num_tokens = [len(tokens) for tokens in x_train_tokens + x_test_tokens]
num_tokens = np.array(num_tokens)

#The average number of tokens in a sequence is:
np.mean(num_tokens)

# The maximum number of tokens in a sequence is:
np.max(num_tokens)

#The max number of tokens we will allow is set to the average plus 2 standard deviations.
max_tokens = np.mean(num_tokens) + 2 * np.std(num_tokens)
max_tokens = int(max_tokens)
max_tokens

#This covers about 95% of the data-set.
np.sum(num_tokens < max_tokens) / len(num_tokens)

#Padding and Truncating
pad = 'pre'
x_train_pad = pad_sequences(x_train_tokens, maxlen=max_tokens,
                            padding=pad, truncating=pad)
x_test_pad = pad_sequences(x_test_tokens, maxlen=max_tokens,
                           padding=pad, truncating=pad)
#We have now transformed the training-set into one big matrix of integers (tokens) with this shape:
x_train_pad.shape

#The matrix for the test-set has the same shape:
x_test_pad.shape

#For example, we had the following sequence of tokens above:
np.array(x_train_tokens[1])
x_train_pad[1]


#Tokenizer Inverse Map
idx = tokenizer.word_index
inverse_map = dict(zip(idx.values(), idx.keys()))

#Helper-function for converting a list of tokens back to a string of words.
def tokens_to_string(tokens):
    # Map from tokens back to words.
    words = [inverse_map[token] for token in tokens if token != 0]
    
    # Concatenate all words.
    text = " ".join(words)

    return text

#For example, this is the original text from the data-set:
x_train_text[1]

#We can recreate this text except for punctuation and other symbols, by converting the list of tokens back to words:
tokens_to_string(x_train_tokens[1])


# Create the Recurrent Neural Network
model = Sequential()
embedding_size = 8

model.add(Embedding(input_dim=num_words,
                    output_dim=embedding_size,
                    input_length=max_tokens,
                    name='layer_embedding'))

model.add(GRU(units=16, return_sequences=True))
model.add(GRU(units=8, return_sequences=True))
model.add(GRU(units=8, return_sequences=True))
model.add(GRU(units=4))
model.add(Dense(1, activation='sigmoid'))

optimizer = Adam(lr=1e-3)

model.compile(loss='binary_crossentropy',
              optimizer=optimizer,
              metrics=['accuracy'])

model.summary()

# Train the Recurrent Neural Network
#with tf.device('/gpu:0'):
model.fit(x_train_pad, y_train, validation_split=0.05, epochs=5, batch_size=16)

# Performance on Test-Set
result = model.evaluate(x_test_pad, y_test, batch_size=16)
print("Accuracy: {0:.2%}".format(result[1]))

# Example of Mis-Classified Text
y_pred = model.predict(x=x_test_pad[0:1000])
y_pred = y_pred.T[0]

import cloudpickle
import tensorflow as tf
import os
from sklearn.preprocessing import LabelBinarizer
import pickle


tf.keras.models.save_model(
                model,
                os.path.join("model/", "tf_sentiment_model.hd5")

            )

print("TF Model written out to {}"
                      .format(os.path.join("model/", "tf_sentiment_model.hd5")))


model2 = tf.keras.models.load_model(
    "model/tf_sentiment_model.hd5",
    custom_objects=None,
    compile=True
)

y_pred = model2.predict(x=x_test_pad[0:1000])
y_pred = y_pred.T[0]

# New Data
text1 = "Bu bankayı hiç beğenmedim."
text2 = "seni sikerim piç köpek soyu"
text3 = "amına bile korum orospu çocuğu"
text4 = "hadi ordan yavşak"
text5 = "çok iyi bir banka"
text6 = "faiz oranları çok düşük"
text7 = "hizmet kaliteniz çok kötü"
text8 = "en iyi banka tabiki ing"
text9 = "faiz oranlarınız çok iyi"
text10 = "faiz oranlarınız çok düşük"
texts = [text1, text2, text3, text4, text5, text6, text7, text8, text9, text10]

# New Data
text1 = "Bu bankayı hiç beğenmedim."
text2 = "seni sikerim piç köpek soyu"
text3 = "amına bile korum orospu çocuğu"
text4 = "hadi ordan yavşak"
text5 = "çok iyi bir banka"
text6 = "faiz oranları çok düşük"
text7 = "hizmet kaliteniz çok kötü"
text8 = "en iyi banka tabiki ing"
text9 = "faiz oranlarınız çok iyi"
text10 = "faiz oranlarınız çok düşük"
text11 = "seni seviyoruz ingo"
text12 = "ingo candır"
text13 = "bugün bankaya bi gidicem"
text14 = "sizi çok seviyorum"
text15 = "bugün neye yatırım yapsam"
text16 = "bugün ne giysem"
text17 = "parayı faize mi yatırsam"
text18 = "en yakın atm nerde"
text19 = "en yakın şube nerde"
text20 = "aq"
text21 = "eft yapmak istiyorum"
text22 = "havale yapmak istiyorum"
text23 = "para göndermek istiyorum"
text24 = "konut faizleri çok düşük"
texts = [text1, text2, text3, text4, text5, text6, text7, text8, text9, text10, 
         text11, text12, text13, text14, text15, text16, text17, text18, text19, 
         text20, text21, text22, text23, text24]

tokens = tokenizer.texts_to_sequences(texts)

tokens_pad = pad_sequences(tokens, maxlen=max_tokens,
                           padding=pad, truncating=pad)
tokens_pad.shape

model.predict(tokens_pad)

model2.predict(tokens_pad)

