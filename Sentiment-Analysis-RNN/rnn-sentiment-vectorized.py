# -*- coding: utf-8 -*-
"""
Created on Sat Sep 15 14:40:14 2018

@author: OKAN
"""

import matplotlib.pyplot as plt
import tensorflow as tf
import numpy as np
from scipy.spatial.distance import cdist

# We need to import several things from Keras.
from tensorflow.python.keras.models import Sequential
from tensorflow.python.keras.layers import Dense, GRU, Embedding
from tensorflow.python.keras.optimizers import Adam
from tensorflow.python.keras.preprocessing.text import Tokenizer
from tensorflow.python.keras.preprocessing.sequence import pad_sequences

print(tf.__version__)
print(tf.keras.__version__)

import pandas as pd
import numpy as np

df = pd.read_csv('sentimentdataset.csv', encoding ='utf-8', sep=',')

from sklearn.model_selection import train_test_split

x_train_text, x_test_text, y_train, y_test = train_test_split(df['yorum'], df['Positivity'], random_state = 0, test_size=0.05,shuffle=True)
print(x_train_text.head())
print('\n\nX_train shape: ', x_train_text.shape)


print("Train-set size: ", len(x_train_text))
print("Test-set size:  ", len(x_test_text))

#Combine into one data-set for some uses below.
frames = [x_train_text,x_test_text]
data_text = pd.concat(frames)

#Print an example from the training-set to see that the data looks correct.
x_train_text[1]

y_train[1]

#Tokenizer

num_words = 10000

#We can then use the tokenizer to convert all texts in the training-set to lists of these tokens.
#x_train_tokens = tokenizer.texts_to_sequences(x_train_text)

#CountVectorizer'ı başlatıyoruz ve eğitim verilerimize uyguluyoruz.
from sklearn.feature_extraction.text import CountVectorizer
vect_train = CountVectorizer(encoding ='iso-8859-9').fit(x_train_text)
vect_test = CountVectorizer(encoding ='iso-8859-9').fit(x_test_text)

#X_train'deki belgeleri bir belge terim matrisine dönüştürürüz
x_train_tokens = vect_train.transform(x_train_text)
x_test_tokens = vect_test.transform(x_test_text)

#For example, here is a text from the training-set:
#x_train_text[1]

# This text corresponds to the following list of tokens:
#np.array(x_train_tokens[1])

#We also need to convert the texts in the test-set to tokens.
#x_test_tokens = tokenizer.texts_to_sequences(x_test_text)
x_train_tokens.shape[0]
x_test_tokens.shape[0]
x_train_tokens
x_test_tokens


#Padding and Truncating Data
#First we count the number of tokens in all the sequences in the data-set.
#num_tokens = [len(tokens) for tokens in x_train_tokens[0] + x_test_tokens[1]]
#num_tokens = np.array(num_tokens)

#The average number of tokens in a sequence is:
#np.mean(num_tokens)

# The maximum number of tokens in a sequence is:
#np.max(num_tokens)

#The max number of tokens we will allow is set to the average plus 2 standard deviations.
#max_tokens = np.mean(num_tokens) + 2 * np.std(num_tokens)
#max_tokens = int(max_tokens)
#max_tokens


# Create the Recurrent Neural Network
model = Sequential()
embedding_size = 8

model.add(Embedding(input_dim=num_words,
                    output_dim=embedding_size,
                    name='layer_embedding'))

model.add(GRU(units=16, return_sequences=True))
model.add(GRU(units=8, return_sequences=True))
model.add(GRU(units=8, return_sequences=True))
model.add(GRU(units=4))
model.add(Dense(1, activation='sigmoid'))

optimizer = Adam(lr=1e-3)

model.compile(loss='binary_crossentropy',
              optimizer=optimizer,
              metrics=['accuracy'])

model.summary()

# Train the Recurrent Neural Network
#with tf.device('/gpu:0'):
model.fit(x_train_tokens, y_train, validation_split=0.05, epochs=5, batch_size=16)

# Performance on Test-Set
result = model.evaluate(x_test_tokens, y_test, batch_size=16)
print("Accuracy: {0:.2%}".format(result[1]))

# Example of Mis-Classified Text
y_pred = model.predict(x=x_test_tokens[0:1000])
y_pred = y_pred.T[0]

import cloudpickle
import tensorflow as tf
import os
from sklearn.preprocessing import LabelBinarizer
import pickle


tf.keras.models.save_model(
                model,
                os.path.join("model-new/", "tf_sentiment_model.hd5")

            )

print("TF Model written out to {}"
                      .format(os.path.join("model-new/", "tf_sentiment_model.hd5")))


model2 = tf.keras.models.load_model(
    "model-new/tf_sentiment_model.hd5",
    custom_objects=None,
    compile=True
)


# New Data
text1 = "Bu bankayı hiç beğenmedim."
text2 = "seni sikerim piç köpek soyu"
text3 = "amına bile korum orospu çocuğu"
text4 = "hadi ordan yavşak"
text5 = "çok iyi bir banka"
text6 = "faiz oranları çok düşük"
text7 = "hizmet kaliteniz çok kötü"
text8 = "en iyi banka tabiki ing"
text9 = "faiz oranlarınız çok iyi"
text10 = "faiz oranlarınız çok düşük"
texts = [text1, text2, text3, text4, text5, text6, text7, text8, text9, text10]



model.predict(vect_train.transform(texts))

model2.predict(vect_train.transform(texts))

