import uuid

from common.database import Database
import models.predicts.constants as PredictConstants
import models.predicts.errors as PredictErrors


class Predict(object):
    def __init__(self, text, normalized_text, score, sentiment, polarity, polarity_score,
                 meaning, time, date, changed_score=None, _id=None):
        self.text = text
        self.normalized_text = normalized_text
        self.score = score
        self.changed_score = changed_score
        self.sentiment = sentiment
        self.polarity = polarity
        self.polarity_score = polarity_score
        self.meaning = meaning
        self.time = time
        self.date = date
        self._id = uuid.uuid4().hex if _id is None else _id

    def __repr__(self):
        return "<Predict {}>".format(self.text)

    def json(self):
        return {
            "_id": self._id,
            "text": self.text,
            "normalized_text": self.normalized_text,
            "score": self.score,
            "changed_score": self.changed_score,
            "sentiment": self.sentiment,
            "polarity": self.polarity,
            "polarity_score": self.polarity_score,
            "meaning": self.meaning,
            "time": self.time,
            "date": self.date
        }

    def api_json(self):
        return {
            "text": self.text,
            "normalized_text": self.normalized_text,
            "score": self.score,
            "changed_score": self.changed_score,
            "sentiment": self.sentiment,
            "polarity": self.polarity,
            "polarity_score": self.polarity_score,
            "meaning": self.meaning,
            "time": self.time
        }

    @classmethod
    def get_by_id(cls, id):
        return cls(**Database.find_one(PredictConstants.COLLECTION, {"_id": id}))

    def save_to_mongo(self):
        Database.update(PredictConstants.COLLECTION, {"_id": self._id}, self.json())

    @classmethod
    def all(cls):
        return [cls(**elem) for elem in Database.find(PredictConstants.COLLECTION, {})]

    def delete(self):
        Database.remove(PredictConstants.COLLECTION, {"_id": self._id})
