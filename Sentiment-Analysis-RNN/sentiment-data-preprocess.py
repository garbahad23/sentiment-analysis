#Kütüphanelerin eklenmesi
import pandas as pd
import numpy as np

#Veri setinin eklenip başlığının belirlenmesi
column = ['yorum']
df = pd.read_csv('yorumlar.csv', encoding ='iso-8859-9', sep='"')
df.columns=column
df.info()
df.head()

#pozitif kelimelerden oluşan kelime seti
dp = pd.read_csv('pozitif_kelimeler_v2.csv', encoding ='utf-8', sep='"')
dp.columns = column
dp.info()
dp.head()

#negatif kelimelerden oluşan kelime seti
dn = pd.read_csv('negatif_kelimeler_v2.csv', encoding ='utf-8', sep='"')
dn.columns = column
dn.info()
dn.head()

#banka sentiment kelimeleri pos,notr,neg
dbs = pd.read_csv('bank-dataset-sentiment.csv', encoding ='utf-8', sep=',')
dbs.info()
dbs.head()


#Veri setindeki Türkçe dolgu kelimelerinin kaldırılması
def remove_stopwords(df_fon):
    stopwords = open('turkce-stop-words', 'r').read().split()
    #df_fon['stopwords_removed'] = list(map(lambda doc:
        #[word for word in doc if word not in stopwords], df_fon['yorum']))
    df_fon['yorum'] = df_fon['yorum'].apply(lambda x: ' '.join([word for word in x.split() if word not in (stopwords)]))
    
        
remove_stopwords(df)

#Verisetinde Positivity adlı bir sütunun oluşturalım ve 
#başlangıçta tüm değerlere olumlu olarak 1 değerinin atayalım
df['Positivity'] = 1

#Veri setimizde 10003. veri ve sonrasındaki veriler olumsuz verilerdir bu yüzden bu değerlerin
#Positivity değerleri 0 ile değiştirelim.
df.Positivity.iloc[10003:] = 0

#df = df.drop(columns=['stopwords_removed'])
dp['Positivity'] = 1
dn['Positivity'] = 0

frames = [df,dn,dp,dbs]
df = pd.concat(frames)

# Export whole set of sentiment analysis data
df.to_csv("sentimentdataset.csv",index=False)
ds = pd.read_csv('sentimentdataset.csv', encoding ='utf-8', sep=',')