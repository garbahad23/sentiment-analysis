# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
import re

df = pd.read_excel('Sentiment-Bank-Data.xlsx', encoding ='iso-8859-9', sep='"')

# Positive kelimeler
dpos = df[1]
dpos = dpos.dropna(axis=0)
dpos.to_csv("pozitif_bank_kelimeler_v1.csv",index=False)

column = ['yorum']
dpos = pd.read_csv('pozitif_bank_kelimeler_v1.csv', encoding ='utf-8', sep='"')
dpos.columns = column
dpos.info()
dpos.head()
dpos['Positivity'] = 1

# Negatif kelimeler
dneg = df[-1]
dneg = dneg.dropna(axis=0)
dneg.to_csv("negatif_bank_kelimeler_v1.csv",index=False)

column = ['yorum']
dneg = pd.read_csv('negatif_bank_kelimeler_v1.csv', encoding ='utf-8', sep='"')
dneg.columns = column
dneg.info()
dneg.head()
dneg['Positivity'] = -1

# Notr kelimeler
dnot = df[0]
dnot = dnot.dropna(axis=0)
dnot.to_csv("notr_bank_kelimeler_v1.csv",index=False)

column = ['yorum']
dnot = pd.read_csv('notr_bank_kelimeler_v1.csv', encoding ='utf-8', sep='"')
dnot.columns = column
dnot.info()
dnot.head()
dnot['Positivity'] = 0


# tüm bank data yı birleştir
frames = [dpos,dneg,dnot]
df = pd.concat(frames)
df = df.reset_index(drop=True)

# clean the dataset
df['yorum'] = df['yorum'].str.strip().replace(u'I',u'ı').replace(u'İ',u'i')
df['yorum'] = df['yorum'].str.lower()
df['yorum'] = df['yorum'].str.replace('[-()\"#\@;:<>{}+=~.?|,]','')
    

# stop word leri çıkardık  
## Veri setindeki Türkçe dolgu kelimelerinin kaldırılması
stopwords = open('turkce-stop-words', 'r').read().split()
df['yorum'] = df['yorum'].apply(lambda x: ' '.join([word for word in x.split() if word not in (stopwords)]))

# Boş datalar row dan çıkarıldı
df['yorum'] = df['yorum'].replace('', np.nan)
df = df.dropna(subset=['yorum'])

df.to_csv("bank-dataset-sentiment.csv",index=False)
