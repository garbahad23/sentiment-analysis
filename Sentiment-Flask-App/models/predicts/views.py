from flask import Blueprint, render_template, request, json, url_for, jsonify
from werkzeug.utils import redirect

from models.predicts.predict import Predict

predict_blueprint = Blueprint('predicts', __name__)


@predict_blueprint.route('/')
def index():
    predicts = Predict.all()
    return render_template('predicts/predict_list.html', predicts=predicts)


@predict_blueprint.route('/detail/<string:id>', methods=['GET', 'POST'])
def detail_page(id):
    predict = Predict.get_by_id(id)
    return render_template('predicts/predict_detail.html', predict=predict)


@predict_blueprint.route('/edit/<string:id>/<string:score>', methods=['GET', 'POST'])
def edit_page(id, score):
    try:
        if str(score) != 'None':
            final_score = int(score)
        else:
            final_score = None

        predict = Predict.get_by_id(id)
        predict.changed_score = final_score
        predict.save_to_mongo()

    except ValueError:
        pass

    return redirect(url_for('.detail_page', id=id))

