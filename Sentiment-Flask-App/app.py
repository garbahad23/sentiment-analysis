from flask import Flask, render_template, request, url_for, jsonify
from flask_bootstrap import Bootstrap
from flask_restful import Api, Resource
from werkzeug.utils import redirect

import pandas as pd
import numpy as np

# NLP
from textblob import TextBlob
import textprocess.kelime_bol as bol
import textprocess.kelimelerine_ayir as ayir
import time
import datetime
import uuid

from common.database import Database
from common.logistic_model import LogisticModel
from common.rnn_model import RnnModel
from common.utils import Utils

from models.predicts.predict import Predict
from models.predicts.views import predict_blueprint


app = Flask(__name__)
Bootstrap(app)
api = Api(app)


# zemberek sentence normalization
import jpype
zemberekApi = None


with app.app_context():
    zemberekpath = "./zemberek_python/zemberek-full.jar"
    jpype.startJVM(jpype.get_default_jvm_path(), "-Djava.class.path=" + zemberekpath, "-ea")
    #jpype.attachThreadToJVM()
    Zemberek = jpype.JClass("org.ingbank.zemberek.Zemberek")
    # zemberekApi = Zemberek()
    zemberekApi = Zemberek()

    # initialize database and models
    Utils.initialize()
    Database.initialize()
    LogisticModel.initialize()
    RnnModel.initialize()


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/predicted', methods=['GET'])
def predicted():
    cursor = Predict.find({}, {'Text': 1, 'Normalized_Text': 1, 'Score': 1, 'Date': 1, '_id': 0}).sort('Date', -1)
    df_table = pd.DataFrame(list(cursor))

    return render_template('predicted.html', df_table=df_table)


@app.route('/analyse', methods=['POST'])
def analyse():
    start = time.time()
    if request.method == 'POST':
        originaltext = request.form['rawtext']
        # if original text is empty do nothing
        if str(originaltext).strip() == '':
            return redirect(url_for('index'))

        # preprocess text
        rawtext = str(originaltext).strip().replace(u"I", u"ı").replace(u"İ", u"i")
        rawtext = rawtext.lower()

        # zemberek sentence normalization
        jpype.attachThreadToJVM()
        rawtext = zemberekApi.sentenceNormalizer(rawtext)

        # remove stop words - stop word de gereksiz kelimeler çıkıcak !!!
        rawtext = Utils.remove_stopwords(rawtext)

        # refresh blob object
        blob = TextBlob(rawtext)

        # positive and negative meaning of the word - meaninde de negatif pozitif kelimeleri ekle
        wordList = list(blob.words)
        meaning = ayir.anlam(wordList)

        # NLP Stuff goes here
        sentiment_prediction = LogisticModel.logistic_predict(rawtext)
        polarity_prediction = RnnModel.rnn_predict(rawtext)

        sentimentScore = Utils.calculate_sentiment_score(meaning, sentiment_prediction[0], polarity_prediction[0][0])

        end = time.time()
        final_time = end - start

        try:
            # save predicted result to mongodb
            predictObj = Predict(text=originaltext, normalized_text=rawtext, score=int(sentimentScore),
                             changed_score=None, sentiment=int(sentiment_prediction[0]),
                             polarity=str(polarity_prediction[0][0]), polarity_score=int(polarity_prediction[0][0] * 100),
                             meaning=int(meaning), time=str(final_time), date=datetime.datetime.utcnow(),
                             _id=uuid.uuid4().hex)
            predictObj.save_to_mongo()
        except:
            pass

    return render_template('index.html', received_text=rawtext,
                           meaning=meaning,sentiment=sentiment_prediction[0],
                           polarity=polarity_prediction[0][0], score=sentimentScore,
                           summary='', final_time=final_time)


def generateReturnDictionary(status, msg):
    retJson = {
        "status": status,
        "msg": msg
    }
    return retJson


class GetSentiment(Resource):
    def post(self):
        start = time.time()

        postedData = request.get_json()
        originaltext = postedData["text"]
        # if original text is empty do nothing
        if str(originaltext).strip() == '':
            return generateReturnDictionary(301, "Invalid Text")

        # preprocess text
        rawtext = str(originaltext).strip().replace(u"I", u"ı").replace(u"İ", u"i")
        rawtext = rawtext.lower()

        # zemberek sentence normalization
        jpype.attachThreadToJVM()
        rawtext = zemberekApi.sentenceNormalizer(rawtext)

        # remove stop words
        rawtext = Utils.remove_stopwords(rawtext)

        # refresh blob object
        blob = TextBlob(rawtext)

        # positive and negative meaning of the word
        wordList = list(blob.words)
        meaning = ayir.anlam(wordList)

        # NLP Stuff goes here
        sentiment_prediction = LogisticModel.logistic_predict(rawtext)
        polarity_prediction = RnnModel.rnn_predict(rawtext)

        sentimentScore = Utils.calculate_sentiment_score(meaning, sentiment_prediction[0], polarity_prediction[0][0])

        end = time.time()
        final_time = end - start

        try:
            # save predicted result to mongodb
            predictObj = Predict(text=originaltext, normalized_text=rawtext, score=int(sentimentScore),
                             changed_score=None, sentiment=int(sentiment_prediction[0]),
                             polarity=str(polarity_prediction[0][0]), polarity_score=int(polarity_prediction[0][0] * 100),
                             meaning=int(meaning), time=str(final_time), date=datetime.datetime.utcnow(),
                             _id=uuid.uuid4().hex)
            predictObj.save_to_mongo()
        except:
            pass

        return jsonify(predictObj.api_json())


# Add Api Resources and Blueprints
api.add_resource(GetSentiment, '/sentiment')
app.register_blueprint(predict_blueprint, url_prefix="/predicts")
